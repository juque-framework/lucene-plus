# lucene-plus

#### 介绍
基于lucene进行通用CRUD的封装，享受lucene丝滑般的操作。

#### 软件架构
1. lucene-plus依赖spring-boot${2.3.3.RELEASE};
2. lucene-plus依赖lucene${8.10.0}
3. 工具类依赖hutool${5.4.0}


#### 安装教程

1.  pom.xml添加依赖

```
  <parent>
    <artifactId>framework</artifactId>
    <groupId>cn.juque</groupId>
    <version>1.0-SNAPSHOT</version>
  </parent>
   <dependency>
      <groupId>cn.juque</groupId>
      <artifactId>lucene-plus</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
   <dependency>
      <groupId>cn.juque</groupId>
      <artifactId>framework-common</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
```

2.  application-${profile}.xml文件添加配置：

```
# 指定索引所在目录
lucene:
  index:
    directory: D:\DOC\multiFile\index\
```

3.  添加lucene-plus的扫描目录：

```
@ComponentScans(value = {@ComponentScan("cn.juque.luceneplus"), @ComponentScan("cn.hutool.extra.spring")})
```


#### 使用说明

1.  定义索引模板，实现接口：IndexTemplate

```
@Component("indexFileHandler")
public class IndexFileHandler implements IndexTemplate {

    @Override
    public String getTemplate() {
        return "{\n"
            + "    \"indexName\":\"file_info\",\n"
            + "    \"fieldDTOList\":[\n"
            + "        {\n"
            + "            \"fieldName\":\"module_id\",\n"
            + "            \"fieldType\":\"STRING\",\n"
            + "            \"value\":\"${module_id}\",\n"
            + "            \"store\":\"YES\"\n"
            + "        },\n"
            + "        {\n"
            + "            \"fieldName\":\"server_path\",\n"
            + "            \"fieldType\":\"STRING\",\n"
            + "            \"value\":\"${server_path}\",\n"
            + "            \"store\":\"YES\"\n"
            + "        },\n"
```

2.  lucene 的CRUD操作统一封装在DocumentPlusService.java

```
    @Test
    public void searchByPage() throws IOException {
        BooleanQuery.Builder builder = new Builder();
        Term term = new Term(IndexFileEnum.SERVER_FILE_NAME.getName(), "*.png*");
        WildcardQuery wildcardQuery = new WildcardQuery(term);
        builder.add(wildcardQuery, Occur.MUST);
        DataGrid<Document> list = this.documentPlusService.searchByPage(IndexFileEnum.FILE_INFO.getName(), builder, new PageInfo());
        Assert.assertNotNull(list);
    }
```

3.  源码说明参考博客：
- 【lucene-plus】初始化索引: https://blog.csdn.net/shenya2/article/details/120680142
- 【lucene-plus】索引初始化的实现逻辑：https://blog.csdn.net/shenya2/article/details/120694670
- 【lucene-plus】保存文档的实现逻辑：https://blog.csdn.net/shenya2/article/details/120713262
- 【lucene-plus】更新文档的实现逻辑：https://blog.csdn.net/shenya2/article/details/120754611
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
