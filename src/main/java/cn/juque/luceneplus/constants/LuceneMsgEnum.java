package cn.juque.luceneplus.constants;

import cn.juque.common.constants.BaseMessageEnum;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/4 1:20
 **/
public enum LuceneMsgEnum implements BaseMessageEnum {

    /**
     * ENUM
     */
    INDEX_NAME_MUST_NOT_NULL("index.name.must.not.null", "索引名称不能为空"),
    FIELD_NUM_MAX_LIMIT("field.num.max.limit", "字段数量超过最大限制"),
    OPEN_INDEX_ERROR("open.index.error", "打开索引异常"),
    SEARCH_PAGE_MAX_LIMIT_ERROR("search.page.max.limit.error", "索引分页查询超过分页最大限制"),
    ID_BELONG_SYSTEM_FIELD("_id.belong.system.field", "'_id'属于系统占用字段"),
    TIMESTAMP_BELONG_SYSTEM_FIELD("_timestamp.belong.system.field", "'_timestamp'属于系统占用字段"),
    BASIC_INDEX_BELONG_SYSTEM_FIELD("basic.index.belong.system.field", "'basic_index'属于系统占用字段"),
    ;

    private String code;

    private String msg;

    LuceneMsgEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 消息码
     *
     * @return String
     */
    @Override
    public String getCode() {
        return this.code;
    }

    /**
     * 消息报文
     *
     * @return String
     */
    @Override
    public String getMsg() {
        return this.msg;
    }
}
