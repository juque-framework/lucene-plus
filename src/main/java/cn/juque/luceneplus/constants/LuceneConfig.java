package cn.juque.luceneplus.constants;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/8 13:36
 **/
public class LuceneConfig {

    /**
     * 分页检索文档的最大数量
     */
    public static final Integer DOC_MAX_COUNT = 200000;

    /**
     * 字段最大数量
     */
    public static final Integer FIELD_MAX_COUNT= 1000;
}
