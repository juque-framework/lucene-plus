package cn.juque.luceneplus.constants;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/8 14:15
 **/
public enum AnalyzerEnum {

    /**
     * 标准分词器
     */
    STANDARD_ANALYZER,
    /**
     * 中文分词器
     */
    SMART_CHINESE_ANALYZER,
    /**
     * 空格分词器
     */
    WHITESPACE_ANALYZER,
    ;
    AnalyzerEnum() {
    }



    public static Analyzer forAnalyzer(String name) {
        AnalyzerEnum analyzerEnum = AnalyzerEnum.valueOf(name);
        Analyzer analyzer;
        switch (analyzerEnum) {
            case SMART_CHINESE_ANALYZER:
                analyzer = new SmartChineseAnalyzer();
                break;
            case WHITESPACE_ANALYZER:
                analyzer = new WhitespaceAnalyzer();
                break;
            default:
                analyzer = new StandardAnalyzer();
        }
        return analyzer;
    }
}
