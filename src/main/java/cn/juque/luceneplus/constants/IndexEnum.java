package cn.juque.luceneplus.constants;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/3 15:05
 **/
public enum IndexEnum {
    /**
     * enum
     *
     */
    TIMESTAMP("_timestamp", "时间戳"),
    BASIC_INDEX("basic_index", "基础索引"),
    ;
    private String name;

    private String label;

    IndexEnum(String name, String label) {
        this.name = name;
        this.label = label;
    }

    public String getName() {
        return name;
    }
}
