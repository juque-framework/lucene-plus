package cn.juque.luceneplus.constants;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/4 1:06
 **/
public enum BasicIndexFieldEnum {

    /**
     * enum
     */
    INDEX_NAME("index_name", "索引名称"),
    FIELD_NAME("field_name", "字段名称"),
    FIELD_TYPE("field_type", "字段类型"),
    FIELD_STORE("field_store", "是否存储"),
    FIELD_POINT("field_point", "是否支持范围查询"),
    FIELD_SORT("field_sort", "是否支持排序"),
    FIELD_ANALYZER("field_analyzer", "字段分词器"),
    ;
    private String value;

    private String label;

    BasicIndexFieldEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getValue() {
        return value;
    }
}
