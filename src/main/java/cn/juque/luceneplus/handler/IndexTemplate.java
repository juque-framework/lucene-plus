package cn.juque.luceneplus.handler;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/8 14:31
 **/
public interface IndexTemplate {

    /**
     * 指定索引模板
     * @return String
     */
    String getTemplate();
}
