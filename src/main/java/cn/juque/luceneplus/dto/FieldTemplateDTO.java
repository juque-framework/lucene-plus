package cn.juque.luceneplus.dto;

import cn.juque.luceneplus.constants.AnalyzerEnum;
import cn.juque.luceneplus.constants.FieldTypeEnum;
import lombok.Data;
import org.apache.lucene.document.Field.Store;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/4 0:22
 **/
@Data
public class FieldTemplateDTO {

    /**
     * 字段名
     */
    private String fieldName;

    /**
     * 字段类型
     */
    private FieldTypeEnum fieldType;

    /**
     * 字符值
     */
    private Object value;

    /**
     * 是否存储
     * <li>fieldType: STRING、TEXT</li>
     */
    private Store store;

    /**
     * 是否支持范围查询
     * <li>fieldType: INTEGER、LONG、DOUBLE</li>
     */
    private Boolean point;

    /**
     * 是否支持排序
     */
    private Boolean isSort;

    /**
     * 分词器
     */
    private AnalyzerEnum analyzer;
}
