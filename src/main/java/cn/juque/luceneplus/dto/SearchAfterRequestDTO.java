package cn.juque.luceneplus.dto;

import lombok.Data;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/8 9:10
 **/
@Data
public class SearchAfterRequestDTO {

    /**
     * 索引名称
     */
    private String indexName;

    /**
     * 查询条件
     */
    private BooleanQuery.Builder builder;

    /**
     * 查询起始点
     */
    private ScoreDoc lastScoreDoc;

    /**
     * 排序规则
     */
    private Sort sort;

    /**
     * 每页数量
     */
    private Integer pageSize;
}
