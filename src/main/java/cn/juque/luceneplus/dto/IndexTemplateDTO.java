package cn.juque.luceneplus.dto;

import java.util.List;
import lombok.Data;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/4 0:22
 **/
@Data
public class IndexTemplateDTO {

    /**
     * 索引名称，即索引目录
     */
    private String indexName;

    /**
     * 字段列表
     */
    private List<FieldTemplateDTO> fieldDTOList;
}
