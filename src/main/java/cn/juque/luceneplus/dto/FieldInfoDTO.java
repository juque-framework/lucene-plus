package cn.juque.luceneplus.dto;

import java.util.List;
import java.util.Map;
import lombok.Data;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Field;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/8 15:44
 **/
@Data
public class FieldInfoDTO {

    /**
     * 字段初始化
     */
    private List<Field> fieldList;

    /**
     * 字段对应的分词器
     */
    private Map<String, Analyzer> analyzerMap;
}
