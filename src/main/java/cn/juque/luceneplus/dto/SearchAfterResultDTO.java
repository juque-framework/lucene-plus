package cn.juque.luceneplus.dto;

import java.util.List;
import lombok.Data;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;

/**
 * @author nuoka
 * @version 1.0.0
 * <li>IntelliJ IDEA</li>
 * <li></li>
 * @date 2021/10/7 23:34
 **/
@Data
public class SearchAfterResultDTO {

    /**
     * 结果集
     */
    private List<Document> result;

    /**
     * 查询结果开始点
     */
    private ScoreDoc lastDoc;

    /**
     * 总结果数
     */
    private Long totalHis;
}
